void setPwmFrequency(int pin, int divisor) {
  byte mode;
  if(pin == 5 || pin == 6 || pin == 9 || pin == 10) {
    switch(divisor) {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 64: mode = 0x03; break;
      case 256: mode = 0x04; break;
      case 1024: mode = 0x05; break;
      default: return;
    }
    if(pin == 5 || pin == 6) {
      TCCR0B = TCCR0B & 0b11111000 | mode;
    } else {
      TCCR1B = TCCR1B & 0b11111000 | mode;
    }
  } else if(pin == 3 || pin == 11) {
    switch(divisor) {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 32: mode = 0x03; break;
      case 64: mode = 0x04; break;
      case 128: mode = 0x05; break;
      case 256: mode = 0x06; break;
      case 1024: mode = 0x7; break;
      default: return;
    }
    TCCR2B = TCCR2B & 0b11111000 | mode;
  }
}

/*
  Blink
  Turns on an LED on for one second, then off for one second, repeatedly.

  Most Arduinos have an on-board LED you can control. On the Uno and
  Leonardo, it is attached to digital pin 13. If you're unsure what
  pin the on-board LED is connected to on your Arduino model, check
  the documentation at http://www.arduino.cc

  This example code is in the public domain.

  modified 8 May 2014
  by Scott Fitzgerald
 */

const byte LED_MEGA      = 11;
const byte LED_MINI      = 10;
const byte BUTTON_LEFT   = A1;
const byte BUTTON_RIGHT  = A2;

// the setup function runs once when you press reset or power the board
void setup() {
  pinMode(13, OUTPUT);
  digitalWrite(13, HIGH);
  delay(100);
  digitalWrite(13, LOW);
  delay(100);
  digitalWrite(13, HIGH);
  delay(100);
  digitalWrite(13, LOW);
  delay(100);
  digitalWrite(13, HIGH);
  delay(100);
  digitalWrite(13, LOW);
  delay(100);
  digitalWrite(13, HIGH);
  delay(100);
  digitalWrite(13, LOW);
  delay(100);
  digitalWrite(13, HIGH);
  delay(100);
  digitalWrite(13, LOW);
  delay(100);
  
  // initialize digital pin 13 as an output.
  pinMode(1, INPUT_PULLUP);
  pinMode(2, INPUT_PULLUP);
  pinMode(3, INPUT_PULLUP);
  pinMode(4, INPUT_PULLUP);
  pinMode(5, INPUT_PULLUP);
  pinMode(6, INPUT_PULLUP);
  pinMode(7, INPUT_PULLUP);
  pinMode(8, INPUT_PULLUP);
  pinMode(9, INPUT_PULLUP);
  pinMode(10, OUTPUT);
  digitalWrite(10, LOW);
  pinMode(11, OUTPUT);
  digitalWrite(10, LOW);
  pinMode(12, INPUT_PULLUP);
  
  pinMode(A0, INPUT_PULLUP);
  pinMode(A1, INPUT_PULLUP);
  pinMode(A2, INPUT_PULLUP);
  pinMode(A3, INPUT_PULLUP);
  pinMode(A4, INPUT_PULLUP);
  pinMode(A5, INPUT_PULLUP);
}

// the loop function runs over and over again forever
void loop() {
  while(1) {
    digitalWrite(LED_MINI, LOW);
    digitalWrite(LED_MEGA, LOW);
    while(digitalRead(BUTTON_LEFT) && digitalRead(BUTTON_RIGHT));
    while(!(digitalRead(BUTTON_LEFT) && digitalRead(BUTTON_RIGHT)));
    for (int i = 0; i<32; i++) {
      analogWrite(LED_MINI, i);
      delay(8);
    }
    analogWrite(LED_MINI, 32);
    digitalWrite(LED_MEGA,LOW);
    while(digitalRead(BUTTON_LEFT) && digitalRead(BUTTON_RIGHT));
    while(!(digitalRead(BUTTON_LEFT) && digitalRead(BUTTON_RIGHT)));
    for (int i = 32; i<127; i++) {
      analogWrite(LED_MINI, i);
      analogWrite(LED_MEGA, i>>3);
      delay(2);
    }
    analogWrite(LED_MINI, 127);
    analogWrite(LED_MEGA, 16);
    while(digitalRead(BUTTON_LEFT) && digitalRead(BUTTON_RIGHT));
    while(!(digitalRead(BUTTON_LEFT) && digitalRead(BUTTON_RIGHT)));
    for (int i = 16; i<127; i++) {
      //analogWrite(LED_MINI, i);
      analogWrite(LED_MEGA, i);
      delay(4);
    }
    analogWrite(LED_MINI, 127);
    analogWrite(LED_MEGA, 127);
    while(digitalRead(BUTTON_LEFT) && digitalRead(BUTTON_RIGHT));
    while(!(digitalRead(BUTTON_LEFT) && digitalRead(BUTTON_RIGHT)));
    {
      for (int i = 127; i>0; i--) {
        analogWrite(LED_MEGA, i);
        analogWrite(LED_MINI, i);
        delay(7);
      }
    }
  }
  
}
